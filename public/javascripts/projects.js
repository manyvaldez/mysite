
Vue.use(VueLazyload);
Vue.use(VueResource);

Vue.use(VueLazyload, {
  preLoad: 10,
  loading: 'images/loading.svg',
  attempt: 2,
  // the default is ['scroll', 'wheel', 'mousewheel', 'resize', 'animationend', 'transitionend']
  listenEvents: [ 'wheel' ]
})

var projects = new Vue({
  el: '#app',
  methods: {
    _open_dialog: function(item) {
      this.dialog.title = item.title;
      this.dialog.description = item.description;
      this.dialog.icon = item.icon;
      this.dialog.background = item.background;
      this.dialog.links = item.links;
      this.dialog.labels = item.labels;
      this.dialogStatus = true;
      this.dialog.working = item.working;
    },
    closeDialog: function() {
      this.dialogStatus = false;
    },
    sendMail: function() {
      this.contact.loading = true;
      this.contact.sendMessage = "Sending ...";
      this.$http.post('/mail', {
        name: this.contact.name,
        email: this.contact.email,
        subject: this.contact.subject,
        message: this.contact.message
      }).then(response => {
        console.log(response);
        this.contact.loading = false;
        this.contact.sendMessage = "Send";
        this.contact.name = "";
        this.contact.email = "";
        this.contact.subject = "";
        this.contact.message = "";
        this.contact.result = "The message was sended, I will answer you soon, thanks. 🚀";
        setTimeout(() =>   {
          this.contact.result = '';
        }, 5000);

      })
    }
  },
  data: {
    dialogStatus: false,
    projects: [
      {
        title: "Descriptive Statistic App",
        description: `Mobile App to resolve statistic problems focused to the descriptive statistic in the probability area. \n\nIt contains features as follows:\n- Nice and intuitive interface.\n- Calculation of measure of dispersion.\n- Frecuencies table.\n- Graphic representation.\n- Storage/Export data.`,
        icon: "/images/projects/statistics/icon.webp",
        background: "/images/projects/statistics/background.webp",
        links: [
          {name: "Google Play", url: "https://play.google.com/store/apps/details?id=com.valdez.many.probabilidad&hl=es_MX"}
        ],
        labels: 'Android, Java, Movil'
      },
      {
        title: "Adired",
        description: "Simple web for the adired telecommunications company. It was developed from scratch with design chosen by the company, it was one of the first freelancer jobs that I did.",
        icon: "/images/projects/adired/icon.png",
        background: "/images/projects/adired/background.jpg",
        links: [
          {name: "Web Site", url: "http://adired.com.mx/"}
        ],
        labels: 'Web, HTML/CSS, Javascript'
      },
      {
        title: "Plyt/Cinemaweb",
        description: "Digital entertainment platform. It is a streaming system of movies / series / music and concerts. It is still in development, it is one of the most complex systems in which I have worked and where I have dedicated many hours of development and learning.",
        icon: "/images/projects/plyt/icon.png",
        background: "/images/projects/plyt/background.png",
        links: [
          {name: "Web Site", url: "not-found"}
        ],
        labels: 'NodeJs, HTML/CSS/JS, MySql, Streaming',
        working: true
      },
      {
        title: "Mayadevelop",
        description: "Group of friends and freelancers that we created a company dedicated to the development of custom software where we carried out several projects that were left unfinished, which may be resumed in the future.\n\n'The best learning experience in all the senses I've had.'",
        icon: "/images/projects/mayadevelop/icon.png",
        background: "/images/projects/mayadevelop/background.jpg",
        links: [
          {name: "Facebook", url: "https://www.facebook.com/mayadevelop/"}
        ],
        labels: 'Company, Software Development'
      },
      {
        title: "Esaduanales",
        description: "Website for a company dedicated to import and export of products",
        icon: "/images/projects/esaduanales/icon.png",
        background: "/images/projects/esaduanales/background.jpg",
        links: [
          {name: "Web Site", url: "esaduanales.com"}
        ],
        labels: 'HTML/CSS/JS, PHP, MySql'
      },
      {
        title: "DrawbBot",
        description: "The system was made as a school project. Basically it is a robot capable of drawing figures controlled through an interface of pattern recognition (OpenCV).",
        icon: "/images/projects/drawbot/icon.png",
        background: "/images/projects/drawbot/background.jpg",
        links: [
          {name:"Slides", url: "https://docs.google.com/presentation/d/13ZYoNJFddgCs9HzlMO9iV_kCW1V1A3r8y-b485AiH90/edit?usp=sharing"}
        ],
        labels: 'HTML/CSS/JS, NodeJs, Jhony 5, OpenCV'
      },
      {
        title: "Chefy",
        description: "It was an experiment for a class at the university. Together with some colleagues we created a progressive web platform where chefs and non-chefs could share their recipes and learn new ones. It was interesting because we use technologies that we had never used before.",
        icon: "/images/projects/chefy/icon.png",
        background: "/images/projects/chefy/background.jpg",
        links: [
          {name:'Slides', url: 'https://docs.google.com/presentation/d/195eQ0cx1l8JbG_xeiB2vbH4mZnFeRoqhWBmdAuuhXk0/edit'}
        ],
        labels: 'HTML/CSS/JS, Ruby on Rails, JQuery, MySQL'
      },
      {
        title: "Gready",
        description: "Mobile learning platform, users can share study guides with their classmates and / or friends, which are created using different question designs to be compatible with all types of intelligences. \n\nCurrently the project is paused due to resource issues :/.",
        icon: "/images/projects/gready/icon.png",
        background: "/images/projects/gready/background.jpg",
        links: [
          {name: "Web site", url: "http://gready.com.mx/"}
        ],
        labels: 'HTML/CSS/JS, Android, Java, NodeJS, MySQL',
        working: true
      },
      {
        title: "Motivate",
        description: "Mobile application of personal motivation. It is a catalog of phrases of great personalities that stand out or stood out in different areas. The application sends registered users daily notifications with different phrases so that the motivation never ends.\n\n 'May your motivation never end'",
        icon: "/images/projects/motivate/icon.svg",
        background: "/images/projects/motivate/background.jpeg",
        links: [
          {name: "Web site", url: "/project/motivate"}
        ],
        labels: 'HTML/CSS/JS, Android, Java, NodeJS, MySQL',
        working: true
      },
      {
        title: "Ewally",
        description: "...",
        icon: "/images/projects/ewally/icon.svg",
        background: "/images/projects/ewally/background.jpg",
        links: [
          {name: "Web site", url: "https://ewally.org/"}
        ],
        labels: 'HTML/CSS/JS, Android, IOS, NodeJS, MySQL',
        working: true
      },
    ],
    dialog: {
      title: "Ewally",
      description: "...",
      icon: "/images/projects/ewally/icon.svg",
      background: "/images/projects/ewally/background.jpg",
      links: [
        {name: "Web site", url: "https://ewally.org/"}
      ],
      labels: 'HTML/CSS/JS, Android, IOS, NodeJS, MySQL',
      working: true
    },
    contact: {
      name: "",
      email: "",
      subject: "",
      message: "",
      result: "",
      loading: false,
      sendMessage: 'Send'
    }
  }
})
