var express = require('express');
var router = express.Router();

var nodemailer = require('nodemailer');

var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'valdezzmanuel88@gmail.com',
    pass: 'catapunchis80'
  }
});

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { section: 'Main' });
});

router.get('/resume', function(req,res) {
  res.render('resume', { section : 'Resume'});
})

router.get('/portfolio', function(req,res) {
  res.render('portfolio', { section : 'Portfolio'});
})

router.get('/contact', function(req,res) {
  res.render('contact', { section : 'Contact'});
})

router.post('/mail', function(req,res) {
  console.log(req.body);

    var mailOptions = {
    from: `${req.body.name} 💻 <${req.body.email}>`,
    to: 'valdezzmanuel88@gmail.com',
    subject: `WORK: ${req.body.subject}`,
    text: `- ${req.body.name}\n- ${req.body.email}\n\n ${req.body.message}`
  };

  transporter.sendMail(mailOptions, function(error, info){
    if (error) {
      console.log(error);
    } else {
      res.send("ok");
      console.log('Email sent: ' + info.response);
    }
  });
})

module.exports = router;
